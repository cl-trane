;;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:trane-TEMPLATE.system
  (:use #:common-lisp #:asdf))
(in-package #:trane-TEMPLATE.system)

(defsystem #:trane-TEMPLATE
  :name "Trane TEMPLATE"
  :description "trane-TEMPLATE"
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause, see file COPYING for details"
  :components ((:module #:src :components ((:file "TEMPLATE"))))
  :depends-on ())

(defsystem #:trane-TEMPLATE.test
  :description "Test suite for trane-TEMPLATE"
  :components ((:module #:t :components ((:file "TEMPLATE"))))
  :depends-on (#:trane-TEMPLATE #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :trane-TEMPLATE))))
  "Perform unit tests for trane-TEMPLATE"
  (asdf:operate 'asdf:load-op :trane-TEMPLATE.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam)) :trane-TEMPLATE))
