;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(in-package :cl-user)

(defpackage #:trane-bb.asd
  (:use #:cl #:asdf))

(in-package #:trane-bb.asd)

(asdf:defsystem #:trane-bb
  :name "Trane BB"
  :description "Parser for BBCode."
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause (see file COPYING for details)"
  :components ((:module #:src :components ((:file "bb"))))
  :depends-on (#:meta-sexp))
