;;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:cl-trane.system
  (:use #:common-lisp #:asdf))
(in-package #:cl-trane.system)

(defsystem #:cl-trane
  :name "Trane TEMPLATE"
  :description "cl-trane"
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause, see file COPYING for details"
  :components ()
  :depends-on (#:trane-common #:trane-bb #:trane-taxonomy #:trane-passengers))

(defsystem #:cl-trane.test
  :description "Test suite for cl-trane"
  :components ((:module #:t :components ((:file "TEMPLATE"))))
  :depends-on (#:cl-trane #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :cl-trane))))
  "Perform unit tests for cl-trane"
  (asdf:operate 'asdf:load-op :cl-trane.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam)) :cl-trane))
