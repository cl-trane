;;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:trane-taxonomy.system
  (:use #:common-lisp #:asdf))
(in-package #:trane-taxonomy.system)

(defsystem #:trane-taxonomy
  :name "Trane taxonomy"
  :description "trane-taxonomy"
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause, see file COPYING for details"
  :components ((:module #:src :components ((:file "taxonomy"))))
  :depends-on (#:trane-common #:iterate
                              #+cl-trane.taxonomy.use-cl-store #:cl-store
                              #+cl-trane.taxonomy.use-cl-store #:flexi-streams))

(defsystem #:trane-taxonomy.test
  :description "Test suite for trane-taxonomy"
  :components ((:module #:t :components ((:file "taxonomy"))))
  :depends-on (#:trane-taxonomy #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :trane-taxonomy))))
  "Perform unit tests for trane-taxonomy"
  (asdf:operate 'asdf:load-op :trane-taxonomy.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam)) :trane-taxonomy))
