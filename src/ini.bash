# http://ajdiaz.wordpress.com/2008/02/09/bash-ini-parser/

function cfg.parser ()
{
    _old_IFS="$IFS"               # save $IFS
    IFS=$'\n' && ini=( $(<$1) ) # convert to line-array
    ini=( ${ini[*]//;*/} ) # remove comments `;'
    ini=( ${ini[*]//\#*/} ) # remove comments `#'
    ini=( ${ini[*]/\ =\ /=} ) # remove anything with a space around `=`
    ini=( ${ini[*]/#[/\}$'\n'cfg.section.} ) # set section prefix
        ini=( ${ini[*]/%]/ \(} ) # convert text2function (1)
    ini=( ${ini[*]/=/=\( } ) # convert item to array
    ini=( ${ini[*]/%/ \)} ) # close array parenthesis
    ini=( ${ini[*]/%\( \)/\(\) \{} ) # convert text2function (2)
    ini=( ${ini[*]/%\} \)/\}} ) # remove extra parenthesis
    ini=( ${ini[*]/#\ */} ) # remove blank lines
    ini=( ${ini[*]/#\ */} ) # remove blank lines with tabs
    ini[0]='' # remove first element
    ini[${#ini[*]} + 1]='}' # add the last brace
    # printf "%s\n" ${ini[*]}
    eval "$(echo "${ini[*]}")" # eval the result
    if [ -z "$_old_IFS" ]       # restore old $IFS
        then unset IFS
        else IFS="$_old_IFS"
    fi
}
