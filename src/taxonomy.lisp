;;; Taxonomy Library
;;; Inspiration: http://svn.automattic.com/wordpress/trunk/wp-includes/taxonomy.php

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

;;; TODO:
;;;  - Customizable dao table, support table and support function names (common prefix)

(defpackage #:trane-taxonomy
  (:use #:common-lisp #:trane-common #:iterate #:postmodern)
  (:export #:taxonomy #:taxonomy-name #:taxonomy-slug
           #:item-dao-class #:valued-taxonomy
           #:cl-store-valued-taxonomy #:deftaxonomy #:ensure-taxonomy
           #:ensure-term #:term #:term-text
           #:slug #:term-taxonomy-name
           #:description #:term-taxonomy #:term-synonyms
           #:id #:find-terms #:new-term #:apply-term
           #:term-value #:item-terms #:order-item-terms
           #:unbind-term :term-item-ids #:term-items
           #:setup-taxonomy-sql))

(in-package #:trane-taxonomy)

(defvar *taxonomies* nil
  "A list of defined taxonomies."
  ;; It is supposed to be just a few of them, hash table overhead
  ;; would be too big.
  )

(defclass taxonomy ()
  ((name :initarg :name :reader taxonomy-name)
   (item-dao-class :initarg :item-dao-class :reader item-dao-class))
  (:documentation "Base taxonomy class.

Taxonomy has a name (symbol or string, which is transformed to a
keyword by DEFTAXONOMY), and refers to DAO classes of taxonomy's ITEM.
DAO classes are required to have an integer primary key, accessible by
reader named ID."))

(defmethod print-object ((taxonomy taxonomy) stream)
  (print-unreadable-object (taxonomy stream :type t :identity t)
    (princ (taxonomy-name taxonomy) stream)))

(defun taxonomy-slug (taxonomy)
  "Return name of taxonomy for database or URL (lowercased string).

Applicable to taxonomy objects, symbols and strings."
  (string-downcase (string (if (or (stringp taxonomy)
                                   (symbolp taxonomy))
                               taxonomy
                               (taxonomy-name taxonomy)))))

(defclass valued-taxonomy (taxonomy)
  ((value-encoder-function :initarg :encoder :reader value-encoder-function :initform #'identity)
   (value-decoder-function :initarg :decoder :reader value-decoder-function :initform #'identity))
  (:documentation "Taxonomy where items applied to terms may have values.

Defines encoder and decoder function slots, which are responsible
for translating value from a Lisp value to DB-safe string.  By
default it is identity function, which means values need to be
strings."))

(defun encode-value (taxonomy value)
  "Encode VALUE for database, as TAXONOMY specifies.

TAXONOMY should be a VALUED-TAXONOMY instance, and VALUE should
be any value supported by the taxonomy.  When VALUE is NIL, it is
encoded as database NULL."
  (if value
      (funcall (value-encoder-function taxonomy)
               value)
      :null))

(defun decode-value (taxonomy value)
  "Decode VALUE from database, as TAXONOMY specifies.

TAXONOMY should be a VALUED-TAXONOMY instance."
  (unless (eq :null value)
    (funcall (value-decoder-function taxonomy) value)))

(defmacro deftaxonomy (name (&optional (class 'taxonomy)) &rest args)
  "Defines taxonomy named NAME, with class CLASS and initargs ARGS, and remembers it in *TAXONOMIES*.

NAME is symbol or string, which will be transformed to a keyword anyway."
  `(setf *taxonomies*
         (cons (make-instance ',class
                              :name ,(make-keyword name)
                              ,@args)
               (delete ,(make-keyword name) *taxonomies*
                       :key #'taxonomy-name))))

(defun ensure-taxonomy (taxonomy)
  "If TAXONOMY is a taxonomy object, return it, otherwise find and return taxonomy named TAXONOMY."
  (if (subtypep (class-of taxonomy) (find-class 'taxonomy))
      taxonomy
      (find (make-keyword taxonomy) *taxonomies*
            :key #'taxonomy-name)))

(defclass term ()
  ((term-id :col-type serial :reader id)
   (term-text :col-type (varchar 255) :initarg :text :accessor term-text :documentation "Full text of term")
   (term-slug :col-type (varchar 255) :accessor slug :documentation "URL-friendly version of term text, initially chosen by database")
   (term-taxonomy :col-type (varchar 32) :col-default "" :reader term-taxonomy-name :initarg :taxonomy-name :documentation "Name of term's taxonomy")
   (term-description :col-type (or db-null text) :accessor description :initarg :description :documentation "Textual description of a term"))
  (:metaclass dao-class)
  (:keys term-id)
  (:documentation "Class for a term associated with taxonomy."))

;;; TODO: pathnames (relative and absolute) as EXECUTE* parameters.
(defun setup-taxonomy-sql (&key item)
  "List of SQL statements, suitable for TRANE-COMMON:EXECUTE*, to initialize database for Trane Taxonomy module.

ITEM is an optional arguments that, if given, specifies foreign key
reference on term's associated item_id columns.  It can be a DAO
class, a symbol naming one, or a list literally specifying S-SQL's
foreign key (as for :CREATE-TABLE sql-op :REFERENCES constraint)."
  (list (dao-table-definition 'term)
        `(:alter-table ,(dao-table-name 'term) :add :unique term-text term-taxonomy)
        `(:alter-table ,(dao-table-name 'term) :add :unique term-slug term-taxonomy)
        `(:alter-table ,(dao-table-name 'term) :add :check (:raw "(term_slug SIMILAR TO '[a-z0-9-]+')"))
        `(:create-table
          item-term       ; FIXME:name
          ((item-id :type int ,@ (etypecase item
                                   (null)
                                   ((or symbol class)
                                    `(:references (,(dao-table-name item))))
                                   (list (cons :references item))))
           (term-id :type int :references (term))
           (item-term-value :type (or db-null text))
           (item-term-order :type (or db-null int)))
          (:primary-key item-id term-id))

        ;; Create new term.  Signals error if term exists.
        ;; FIXME: count existing slugs starting with new slug
        "CREATE OR REPLACE FUNCTION new_term(
    the_text VARCHAR, the_slug VARCHAR, the_taxonomy VARCHAR)
RETURNS term
AS $body$
DECLARE
    res term;
    i INTEGER;
BEGIN
    BEGIN
        INSERT INTO term(term_text, term_slug, term_taxonomy)
             VALUES (the_text, the_slug, the_taxonomy);
    EXCEPTION
    WHEN unique_violation THEN
        SELECT count(*) FROM term WHERE term_slug LIKE the_slug||'%' INTO i;
        LOOP
            i = i+1;
            IF i > 999 THEN RAISE EXCEPTION 'Afraid of infinite loop.';
            END IF;
            BEGIN
                INSERT INTO term(term_text, term_slug, term_taxonomy)
                     VALUES (the_text, the_slug||'-'||i, the_taxonomy);
            EXCEPTION WHEN unique_violation THEN
                IF POSITION('_slug_' IN SQLERRM) <> 0 THEN CONTINUE; END IF;
                RAISE EXCEPTION 'Duplicate term.  How to re-raise from PL/PgSQL?';
            END;
            EXIT;               -- Exit loop when no unique_violation.
        END LOOP;
    END;
    SELECT INTO res * FROM term WHERE term_id=CURRVAL('term_term_id_seq');
    RETURN res;
END;
$body$ LANGUAGE plpgsql;"

        ;; Selects or inserts term named `the_text' in taxonomy
        ;; `the_taxonomy'.  Returns found/new term.  No support for
        ;; description or parent for new term, this function is
        ;; intended to work with simple terms only (e.g. tags).
        "CREATE OR REPLACE FUNCTION ensure_term(
    the_text VARCHAR,
    suggested_slug VARCHAR,
    the_taxonomy VARCHAR)
RETURNS term
AS $body$
DECLARE
    res term;
BEGIN
    SELECT INTO res * FROM term
          WHERE term_text=the_text
            AND term_taxonomy=the_taxonomy;
    IF NOT FOUND
    THEN res = new_term(the_text, suggested_slug, the_taxonomy);
    END IF;
    RETURN res;
END;
$body$ LANGUAGE plpgsql;"

        ;; Sets term value for item (UPDATE or INSERT if needed).  When value
        ;; is NULL, just ensures that an association is established.
        "CREATE OR REPLACE FUNCTION set_item_term_value(
       the_item_id INTEGER,
       the_term_id INTEGER,
       the_new_value TEXT)
RETURNS VOID
AS $body$
BEGIN
    UPDATE item_term
           SET item_term_value=the_new_value
           WHERE item_id=the_item_id AND term_id=the_term_id;
    IF NOT FOUND THEN
        INSERT INTO item_term(item_id, term_id, item_term_value)
            VALUES(the_item_id, the_term_id, the_new_value);
    END IF;
END;
$body$ LANGUAGE PLPGSQL;"))

(defmethod print-object ((term term) stream)
  (print-unreadable-object (term stream :type t :identity t)
    (princ (slug term) stream)))

(defun term-taxonomy (term)
  "TERM's taxonomy object"
  (ensure-taxonomy (term-taxonomy-name term)))

(defun ensure-term (taxonomy &key text slug create-p)
  "Find or create term in taxonomy TAXONOMY.

TEXT is a full text of term; if TEXT is given, CREATE-P is
non-NIL and term is not found, new term is inserted into
database.

If SLUG is given instead of TEXT, only search is possible, not
creation."
  #+b0rken (assert (and (or text slug)
                        (not (and text slug)))
                   ()
                   "Either TEXT or SLUG should be given, but not both.")
  (first
   (if text
       (if create-p
           (query-dao 'term (:select '* :from (:ensure-term text (or slug (slugify text)) (taxonomy-slug taxonomy))))
           (select-dao 'term (:and (:= 'term-text text)
                                   (:= 'term-taxonomy (taxonomy-slug taxonomy)))))
       (progn
         (when create-p
           (warn "CREATE-P is meaningful only with TEXT defined."))
         (select-dao 'term (:and (:= 'term-slug slug)
                                 (:= 'term-taxonomy (taxonomy-slug taxonomy))))))))

(defun new-term (taxonomy text &optional parent)
  "Create new term in TAXONOMY, with full name TEXT."
  (first (query-dao 'term
                    (:select '* :from
                             (:new-term text (taxonomy-slug taxonomy) (if parent (slug parent) :null))))))

(defun find-terms-where-clause (&key taxonomy text slug
                                &aux (query ()))
  (when taxonomy
    (push (list := 'term-taxonomy (taxonomy-slug taxonomy))
          query))

  (when text
    (push (list := 'term-text text) query))

  (when slug
    (push (list := 'term-slug slug) query))

  (if (> (length query) 1)
      (push :and query)
      (setf query (first query)))

  query)

(defun find-terms (&key taxonomy text slug)
  "Find list of terms satisfying given keywords."
  (query-dao 'term
             (sql-compile `(:order-by (select '* :from 'term
                                              :where ,(find-terms-where-clause :taxonomy taxonomy
                                                                               :text text
                                                                               :slug slug))
                                      'term-text))))

(defun term-synonyms (term)
  "Return list of terms synonymous with TERM.

Two terms are synonymous when they are in the same taxonomy and differ
only in case.  More special cases for synonymity may be introduced
later.

TERM is always included in the returned list, and it is a first item.

TERM may be also a list, whose first element is taxonomy object or
name, and second element is term name.  This is a case of premature
optimization."
  (etypecase term
    (term (cons term
                (mapcar #'cache-dao 
                        (query-dao 'term
                                   (sql (:select '* :from 'term
                                                 :where (:and (:!= 'term-id (id term))
                                                              (:= 'term-taxonomy
                                                                  (taxonomy-slug (term-taxonomy term)))
                                                              (:= (:lower 'term-text)
                                                                  (:lower (term-text term))))))))))
    (list (mapcar #'cache-dao
                  (query-dao 'term
                             (sql (:select '* :from 'term
                                           :where (:and (:= 'term-taxonomy
                                                            (taxonomy-slug (first term)))
                                                        (:= (:lower 'term-text)
                                                            (:lower (string (second term))))))))))))

(defun apply-term (item term &key value order)
  "Apply TERM to ITEM, optionally setting its value to VALUE."
  (query (:insert-into 'item-term :set
                       'item-id (id item)
                       'term-id (id term)
                       'item-term-value (encode-value (term-taxonomy term) value)
                       'item-term-order (null-or order))))

(defun term-value (item term
                   &aux (encoded (query (:select 'item-term-value 'item-term-order
                                                 :from 'item-term
                                                 :where (:and (:= 'item-id (id item))
                                                              (:= 'term-id (id term))))
                                        :row)))
  "Returns value that association of ITEM and TERM is set to.

As a second value returns T if an association was found at all,
NIL otherwise.  This makes it possible to tell between an
association with a NIL value and no association at all.

Third value is a term ordering value, if ordering is set."
  (if encoded
      (values (decode-value (term-taxonomy term) (first encoded)) t (unless-null (second encoded)))
      (values nil nil nil)))

(defun (setf term-value) (new-value item term)
  "Set new value for association of ITEM and TERM.

New association between ITEM and TERM is established if it was
not present before."
  (query (:select (:set-item-term-value (id item) (id term)
                                        (encode-value  (term-taxonomy term) new-value)))
         :none))

(defun unbind-term (item term)
  "Deletes association between ITEM and TERM."
  (execute (:delete-from 'item-term :where
                         (:and (:= 'item-id (id item))
                               (:= 'term-id (id term)))))
  (execute (:delete-from 'term :where (:and (:= 'term-id (id term))
                                            (:not (:exists (:select '* :from 'item-term
                                                                    :where (:= 'term-id (id term)))))))))

(defun item-terms (item &optional taxonomy)
  "List TERMs associated with ITEM in given TAXONOMY.

If TAXONOMY is not given, returns terms in all taxonomies."
  (query-dao 'term
             (sql-compile
              `(:order-by
                (:select 'term.* :from 'term 'item-term :where
                         (:and ,@(when taxonomy
                                       (list (list :=
                                                   ''term.term-taxonomy
                                                   (taxonomy-slug taxonomy))))
                               (:= 'item-term.item-id ,(id item))
                               (:= 'term.term-id 'item-term.term-id)))
                'item-term.item-term-order))))

(defun (setf item-terms) (new-terms item &optional taxonomy)
  "Resets ITEM's terms to NEW-TERMS.  Restricts to terms in TAXONOMY, if needed.

Doesn't touch values or order of terms that are left.  Sets order of
added terms to NULL."
  (let* ((old-terms (item-terms item taxonomy))
         (new-ids (mapcar #'id new-terms))
         (old-ids (mapcar #'id old-terms))
         (ids-to-del (set-difference old-ids new-ids))
         (ids-to-add (set-difference new-ids old-ids)))
    (with-transaction ()
      (when ids-to-del
        (execute (:delete-from 'item-term :where
                               (:and (:= 'item-id (id item))
                                     (:in 'term-id (:set ids-to-del)))))
        (execute (:delete-from 'term :where (:and (:in 'term-id (:set ids-to-del))
                                                  (:not (:exists (:select '* :from 'item-term
                                                                          :where (:= 'item-term.term-id 'term.term-id))))))))
      (when ids-to-add
        (execute (:insert-rows-into 'item-term
                                    :columns 'item-id 'term-id
                                    :values (loop for id in ids-to-add
                                               collect (list (id item) id))))))))

(defun order-case-ssql (column values)
  `(:case
       ,@(loop
            for i from 1
            for v in values
            collect `((:= ,column ,v) ,i))
     (t :null)))

(defun order-item-terms (item terms &key taxonomy null-other-terms
                         &aux (term-ids (mapcar #'id terms)))
  "Set TERMS (a list of terms or term IDs), associated with ITEM, in given order.

If NULL-OTHER-TERMS is not NIL, terms not listed in TERMS get their
order column set to NULL.  If TAXONOMY is given, restrict changes to
terms of given taxonomy."
  (query
   (sql-compile
    `(:update item-term
              :set item-term-order ,(if terms
                                        (order-case-ssql 'term-id term-ids)
                                        :null)
              :where (:and (:= 'item-id ,(id item))
                           ,@(if null-other-terms
                                 (when taxonomy
                                   `((:exists (:select t
                                                       :from 'term
                                                       :where (:and (:= 'term-id 'item-term.term-id)
                                                                    (:= 'term-taxonomy
                                                                        ,(taxonomy-slug taxonomy)))))))
                                 `((:in 'term-id (:set ,@term-ids)))))))))

(defun term-item-ids (term)
  "IDs of items associated with given TERM."
  (query (:select 'item-id :from 'item-term :where (:= 'term-id (id term)))
         :column))

(defun term-items (term &aux (class (item-dao-class (term-taxonomy (if (integerp term) ; FIXME
                                                                       (get-dao 'term term)
                                                                       term)))) )
  "Items associated with given TERM."
  (mapcar #'(lambda (id)
              (get-dao class id))
          (term-item-ids term)))

#+cl-trane.taxonomy.use-cl-store
(progn
  (defun store-to-base64 (obj)
    (base64:usb8-array-to-base64-string
     (flex:with-output-to-sequence (s)
       (cl-store:store obj s))))

  (defun restore-from-base64 (b64)
    (flex:with-input-from-sequence (s (base64:base64-string-to-usb8-array b64))
      (cl-store:restore  s)))

  (defclass cl-store-valued-taxonomy (valued-taxonomy)
    ()
    (:default-initargs :encoder #'store-to-base64 :decoder #'restore-from-base64)
    (:documentation "Valued taxonomy that by default encodes/decodes almost any Lisp object with CL-STORE as BASE64 string.")))
