;;;; -*- lisp -*- common.lisp -- common functionality used by CL-Trane libraries and projects

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

;; Package for internal use in libraries and projects
(defpackage #:trane-common
  (:use #:common-lisp #:iterate #:puri)
  (:export #:id #:slug #:slug-dao #:slugify
           #:dirtiness-mixin #:dirty-p #:mark-as-dirty #:update-dao-if-dirty
           #:cache-dao #:with-dao-cache
           #:*config* #:init-config #:*db* #:init-db-connection #:execute*
           #:unless-null #:null-or #:qp-utf8 #:invoke-sendmail
           #:report-error #:report-error-by-email #:report-error-to-file #:format-error-report
           #:*error-context-hook* #:*error-report-pathname-defaults*
           #:make-keyword #:named-lambda #:random-string #:salted-password #:hex-md5
           #:*handler-package* #:handler-function
           #:start-hunchentoot))

(in-package #:trane-common)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Common access methods for database objects

(defgeneric id (object)
  (:documentation "Numeric ID or :NULL for DAOs and, if it makes sense, other objects.

Usually it will be a reader method automatically defined for ID column of a DAO."))

;;; To enable using integer as a DAO when only ID is needed
(defmethod id ((i integer))
  i)

;;; NULL database id for NIL object.
(defmethod id ((n null))
  :NULL)

(defmethod id ((n string))
  (parse-integer n :junk-allowed t))

(defgeneric slug (object)
  (:documentation "Get short, unique, urlified name of OBJECT."))

(defun slug-dao (class slug)
  "Select DAO of CLASS having \"slug\" column set to SLUG."
  (first (postmodern:select-dao class (:= 'slug slug))))

;;; FIXME:flatten characters, unicode and so on
(defun slugify (str)
  (iterate (for cs in-string str)
           (for c = (char-downcase cs))
           (for safe-p = (find c "abcdefghijklmnopqrstuvwxyz"))
           (for previous-safe-p previous safe-p initially t)
           (when safe-p
             (unless previous-safe-p
               (collect #\- result-type string))
             (collect c result-type string))))

(defclass dirtiness-mixin ()
  ((dirty-p :initform nil :accessor dirty-p)))

(defun mark-as-dirty (dao)
  (setf (dirty-p dao) t))

(defun update-dao-if-dirty (dao)
  (when (dirty-p dao)
    (postmodern:update-dao dao)
    (setf (dirty-p dao) nil)))

;;; DAO caching
(defvar -dao-cache-)

(defmethod postmodern:get-dao :around (type &rest keys)
  (if (boundp '-dao-cache-)
      (cdr
       (or (assoc (cons type keys) -dao-cache- :test #'equal)
           (first (push (cons (cons type keys)
                              (call-next-method))
                        -dao-cache-))))
      (call-next-method)))

(defun cache-dao (dao)
  "Manually add DAO to cache used by WITH-DAO-CACHE.

Returns DAO"
  (when (boundp '-dao-cache-)
    (push (cons (cons (class-name (class-of dao))
                      (postmodern:dao-keys dao))
                dao)
          -dao-cache-))
  dao)

(defmacro with-dao-cache (&body body)
  "Cache DAOs within dynamic extent of BODY.

Within BODY, DAO objects obtained by GET-DAO are cached to avoid
repeated queries fot the same objects.  DAO objects are added
automatically before being returned by GET-DAO, or can be added
manually with CACHE-DAO (e.g. when obtained by QUERY-DAO)."
  `(let ((-dao-cache- nil))
     ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Initialization protocol

(defvar *config* (py-configparser:make-config)
  "Parsed configuration file.")

(defvar *config-files* (list "config.ini")
  "List of default configuration files.")

(defun init-config (&rest files)
  "Read in the configuration from files, defaulting to ones listed in *CONFIG-FILES*.

Should be first thing called in final init routine."
  (py-configparser:read-files *config* (or files *config-files*)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DB sugar

(defvar *db* nil
  "Database connection specification")

(defmethod hunchentoot:dispatch-request :around (dispatch-table)
  "Take care of database connection during HTTP request."
  (declare (ignore dispatch-table))
  (if *db*
      (postmodern:with-connection *db*
        (call-next-method))
      (call-next-method)))

(defun init-db-connection (&optional (section-name "db") (connect-toplevel-p t))
  "Initialize database connection from config.ini"
  (flet ((opt (name)
           (coerce (py-configparser:get-option *config* section-name name)
                   'simple-string)))
    (setf *db* (list (opt "database")
                     (opt "username")
                     (opt "password")
                     (opt "host")
                     :pooled-p t)))
  (when connect-toplevel-p
    (apply #'postmodern:connect-toplevel (butlast *db* 2))))

(defmethod print-object :around ((timestamp simple-date:timestamp) stream)
  (if *print-escape*
      (call-next-method)
      (multiple-value-bind (year month day hour minute second millisecond)
          (simple-date:decode-timestamp timestamp)
        (declare (ignore millisecond))
        (format stream "~D-~2,'0D-~2,'0D ~2,'0D:~2,'0D:~2,'0D"
                year month day hour minute second))))

(defun execute* (statements &optional (query-log *standard-output*))
  "Execute a possibly nested list of sql STATEMENTS.

STATEMENTS may be either S-SQL expressions, literal SQL strings, or
lists of statements."
  (labels ((execute-stmts (statements)
             (dolist (stmt statements)
               (etypecase stmt
                 (null)                 ; ignore NILs.
                 (string (postmodern:execute stmt))
                 (list (if (keywordp (first stmt))
                           (postmodern:execute (s-sql:sql-compile stmt))
                           (execute-stmts stmt)))))))
    (let ((cl-postgres:*query-log* query-log))
      (execute-stmts statements))))

(defun unless-null (v)
  "If V equals :NULL, return NIL, otherwise return V."
  (unless (eq :null v)
    v))

(defun null-or (v)
  "If V is NIL or an empty string, return :NULL, otherwise return V."
  (if (or (null v) (and (stringp v) (string= "" v)))
      :null
      v))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; URIs

(defmethod uri ((u symbol))
  "URI on keybord -> look up from config."
  (parse-uri (py-configparser:get-option *config* "uri"
                                         (string-downcase (string u)))))

;; Allow merging any URI designators
(defmethod merge-uris (u b &optional p)
  (merge-uris (uri u) (uri b) p))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mailing

(defun qp-utf8 (string)
  "Encode STRING as quoted-printable UTF-8"
  (cl-qprint:encode (map 'string #'code-char (trivial-utf-8:string-to-utf-8-bytes string))))

(defun invoke-sendmail (sender rcpt body-and-headers
                        &key (sendmail-binary (py-configparser:get-option *config* "mailing" "sendmail")) )
  "Invoke sendmail binary.

SENDER is envelope sender address, RCPT is envelope recipient
address (may be a list), BODY-AND-HEADERS is fed to sendmail binary as
stdin and it may be a string or an input stream.  SENDMAIL-BINARY is
full path to sendmail binary, as a string, default is taken from
*CONFIG* section mailing, variable sendmail."
  (flet ((ss (s)
           (coerce s 'simple-string)))
    (external-program:run
     sendmail-binary
     (if (listp rcpt)
         (list* (concatenate 'string "-f" (ss sender)) (mapcar #'ss rcpt))
         (list (concatenate 'string "-f" (ss sender)) (ss rcpt)))
     :input (etypecase body-and-headers
              (stream body-and-headers)
              (string (make-string-input-stream body-and-headers))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Reporting conditions.

(defun format-request-details (&optional stream)
  "Format Hunchentoot request details to STREAM"
  (format stream
          "From ~A:~A ~A to ~A~%~A ~A ~A~%~{  ~S~%~}~@[Request body: ~S~%~]~@[GET params: ~S~%~]~@[POST params: ~S~%~]~@[Cookies: ~S~%~]"
          (hunchentoot:remote-addr) (hunchentoot:remote-port) (multiple-value-list (hunchentoot:real-remote-addr)) (hunchentoot:host)
          (hunchentoot:server-protocol) (hunchentoot:request-method) (hunchentoot:request-uri)
          (hunchentoot:headers-in)
          (hunchentoot:raw-post-data) (hunchentoot:get-parameters) (hunchentoot:post-parameters) (hunchentoot:cookies-in)))

(defun file-timestamp ()
  "Timestamp string to generate unique file names."
  (multiple-value-bind (s n h d m y wd dp zn) (get-decoded-time)
    (declare (ignore wd dp zn))
    (format nil "~4,'0d~2,'0d~2,'0d-~2,'0d~2,'0d~2,'0d"
            y m d h n s)))

(defmacro with-open-file-unique ((stream base-pathname) &body body)
  "Open new unique file for writing and execute BODY with new file opened as STREAM.

File name is named like BASE-PATHNAME, with PATHNAME-TYPE set to time
stamp and (if needed) a unique integer."
  (let ((p (gensym)) (pp (gensym)) (i (gensym)) (tt (gensym)))
    `(let ((,p (pathname ,base-pathname))
           (,tt (file-timestamp))
           (,i nil))
       (loop
          for ,pp = (catch 'loop
                      (let ((,pp (make-pathname :defaults ,p
                                                :type (format nil "~A~@[-~2,'0d~]" ,tt ,i))))
                        (with-open-file (,stream ,pp :direction :output :if-exists nil)
                          (unless ,stream
                            (if ,i (incf ,i) (setf ,i 1))
                            (throw 'loop nil))
                          ,@body
                          ,pp)))
          until ,pp
          finally (return ,pp)))))

(defvar *error-context-hook* nil
  "When set to a function designator, designated function will be
  FUNCALled when generating error report, with condition instance as
  an argument, and result will be inserted into error report as an
  error context.")

(defun format-error-report (s e)
  (format s "Error report from ~A, caught on ~A:~%~%" (machine-instance) (hunchentoot::iso-time))
  (when (boundp 'hunchentoot:*request*)
    (format-request-details s)
    (terpri s))
  (when *error-context-hook*
    (format s "Context: ~A~%"
            (handler-case (funcall *error-context-hook* e)
              (error (ee)
                (format nil "CONTEXT FN ERRED! ~A~%Context backtrace:~%~A"
                        ee (hunchentoot:get-backtrace ee)))))
    (terpri s))

  (format s "Original error: ~A~%~A~%" e (hunchentoot::get-backtrace e)))

(defvar *error-report-pathname-defaults* (merge-pathnames "error")
  "Base pathname for error reports when written to file.

Defaults to \"error\" in *DEFAULT-PATHNAME-DEFAULTS*.")

(defun report-error-to-file (e)
  (with-open-file-unique (s *error-report-pathname-defaults*)
    (format-error-report s e)))

(defun report-error-by-email (e rcpt &optional (sender (py-configparser:get-option *config* "mailing" "sender")))
  (invoke-sendmail sender rcpt
                   (format nil "From: <~A>~%To: <~A>~%Subject: Uncaught error ~A~%Content-Type: text/plain; charset=utf-8~%Content-Transfer-Encoding: Quoted-Printable~%~%~A"
                           sender rcpt e
                           (qp-utf8
                            (with-output-to-string (s)
                              (format-error-report s e))))))

(defun report-error (e &optional (mailto (ignore-errors
                                           (py-configparser:get-option *config* "mailing" "error_mailto"))))
  (if mailto
      (report-error-by-email e mailto)
      (report-error-to-file e)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Hunchentoot

(defun start-hunchentoot (&optional (section-name "hunchentoot"))
  (hunchentoot:start-server
   :port (py-configparser:get-option *config* section-name "port" :type :number)
   ;; SBCL barfs without coerce, WTF?
   :address (coerce (py-configparser:get-option *config* section-name "address")
                    'simple-string)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; General utility functions

(defun make-keyword (str)
  "Convert a string or symbol to a keyword."
  (intern (string-upcase (string str)) :keyword))

(defmacro named-lambda (name args &body body)
  "Version of LAMBDA that returns anonymous function defined with
FLET and named NAME, which it's PRINTed with a name in most Lisp
implementations."
  `(flet ((,name ,args ,@body))
     #',name))

(defun random-string (&key
                      (alphabet "123456790abcdefghijklmnopqrstuvwxyz")
                      (min-length 17)
                      (max-length 40))
  "Simple random string for initial password to use in account activation process."
  (coerce (loop for i from 0 to (+ min-length (random (- max-length min-length)))
             collect (aref alphabet (random (length alphabet))))
          'string))

(defun salted-password (salt password)
  "Return Base64-encoded MD5 checksum of SALT concatenated with PASSWORD."
  (cl-base64:usb8-array-to-base64-string 
   (md5:md5sum-sequence (concatenate 'string salt password))))

(defun hex-md5 (sequence)
  "Return MD5 checksum of SEQUENCE as a hexadecimal string."
  (format nil "~(~{~2,'0x~}~)"
          (coerce (md5:md5sum-sequence sequence)
                  'list)))

(defvar *handler-package* nil
  "Package, in which HANDLER-FUNCTION looks for handlers.

When NIL, *PACKAGE* is assumed, which is probably not what you want.")

(defun handler-function (&rest name-parts)
  "Return handler function whose name consists of NAME-PARTS.

Handler function name is NAME-PARTS joined with slash signes, and
starting with a slash; NIL part means an empty place.  E.g. for
NAME-PARTS :FOO :BAR :BAZ, it's /FOO/BAR/BAZ; for :FOO NIL it's /FOO/;
and for NIL :XYZZY it's //XYZZY.

If symbol with such name exists in *HANDLER-PACKAGE* (or in *PACKAGE*,
if *HANDLER-PACKAGE* is NIL, but probably it's not what you want), and
it names a function, this function is returned."
  (multiple-value-bind (sym type)
      (find-symbol (format nil "~{/~@[~A~]~}" name-parts)
                   (or *handler-package* *package*))
    (when (and (not (eql type :inherited))
               (fboundp sym))
      (let ((fn (symbol-function sym)))
        (when (functionp fn)
          fn)))))
