(defpackage #:trane-dependency-graph
  (:use #:common-lisp #:asdf)
  (:export #:graph))
(in-package #:trane-dependency-graph)

(defvar *traversed*)
(defvar *indirect*)
(defvar *stream*)

(defun trane-component-p (name)
  (or (and (> (length name) 6)
           (string= "trane-" (subseq name 0 6)))))

(defun traverse (system &aux (name (component-name system)))
  (unless (member name *traversed* :test #'string=)
    (push name *traversed*)
    (dolist (dep (mapcar #'find-system (rest (assoc 'load-op (component-depends-on 'load-op system)))))
      (format *stream* "    ~S -> ~S;~%" name (component-name dep))
      (unless (or (string= name "cl-trane")
                  (trane-component-p name))
        (push (component-name dep) *indirect*))
      (traverse dep))))

(defparameter *legend* "
    Legend -> \"CL-Trane component\";
    \"CL-Trane component\" -> \"Direct dependency\";
    \"Direct dependency\" -> \"Indirect dependency\";
    Legend [penwidth=4,style=solid,fontsize=16]
    \"CL-Trane component\" [penwidth=2,style=solid,fontsize=12]
    \"Direct dependency\" [style=solid]
")

(defun graph-to-stream (s)
  (let ((*traversed* nil) (*indirect* nil) (*stream* s))
    (format *stream* "digraph \"cl-trane\" {~%    graph[rankdir=LR]~%    node [fontname=\"Helvetica Neue\",fontsize=10,style=dotted];~%")
    (traverse (find-system :cl-trane))
    (terpri *stream*)
    (dolist (dep *traversed*)
      (cond
        ((trane-component-p dep)
         (format *stream* "    ~S [penwidth=2,style=solid,fontsize=12];~%" dep))
        ((not (member dep *indirect* :test #'string=))
         (format *stream* "    ~S [style=solid];~%" dep))))
    (format *stream* "    \"cl-trane\" [penwidth=4,style=solid,fontsize=16];~%~A}~%"
            *legend*)))

(defun graph (output-file-name)
  (with-open-file (s output-file-name
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (graph-to-stream s)))
