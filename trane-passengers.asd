;;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:trane-passengers.system
  (:use #:common-lisp #:asdf))
(in-package #:trane-passengers.system)

(defsystem #:trane-passengers
  :name "Trane passengers"
  :description "trane-passengers"
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause, see file COPYING for details"
  :components ((:module #:src :components ((:file "passengers"))))
  :depends-on (#:trane-common #:postmodern #:cl-base64))

(defsystem #:trane-passengers.test
  :description "Test suite for trane-passengers"
  :components ((:module #:t :components ((:file "passengers"))))
  :depends-on (#:trane-passengers #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :trane-passengers))))
  "Perform unit tests for trane-passengers"
  (asdf:operate 'asdf:load-op :trane-passengers.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam)) :trane-passengers))
