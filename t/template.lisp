(in-package #:trane-TEMPLATE)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package '#:fiveam))

(def-suite :trane-TEMPLATE
    :description "test suite for trane-TEMPLATE")
(in-suite :trane-TEMPLATE)
